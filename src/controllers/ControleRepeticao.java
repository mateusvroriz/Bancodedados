
package controllers;

import java.util.ArrayList;
import java.util.Arrays;

public class ControleRepeticao<T> extends ArrayList<T>{

    public ControleRepeticao() {
        super(); 
    }

 
    public boolean contains(Object[] obj){
        
        for(int i=0; i< this.size(); i++){
          
            Object[] toCompare = (Object[])this.get(i);
        
            if(Arrays.equals(obj, toCompare)){
                return true;
            }
        }
        return false;
    }
}
